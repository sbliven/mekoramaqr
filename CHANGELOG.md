Changelog
=========

0.1.0
-----
*2017-11-03*

* First official release!
* Add `mekoramaqr` command

0.1.0-rc1
---------
*2017-10-27*

Initial public beta release.

* Reading, writing, and editing Mekorama QR codes
* Supports all official blocks, plus some "hidden" blocks
* Generate PNG cards
* CLI interface (`python -m mekoramaqr -h`)