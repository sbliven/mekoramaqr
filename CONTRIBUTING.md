# Contributing to MekoramaQR

## Testing

To run unit tests:

```
python setup.py test
```

Unit tests should be placed in `mekoramaqr/test` and be called `test_<module>.py`

## Making a release

To make a pypi release, first set up `~/.pypirc` as described
[here](http://peterdowns.com/posts/first-time-with-pypi.html).

Update `__version__` in `mekoramaqr/__init__.py`. Tag the commit with the
version number, e.g.

```
git commit -m "Release v0.0.0"
git tag -a "v0.0.0" --sign -m "Tag v0.0.0"
git push
git push --tags
```

Now upload to pypitest and pypi itself:

```
python setup.py sdist upload -r pypi
```

Afterwards, update `__version__` to the next dev version (`0.0.1-dev1`).

## License

By contributing to MekoramaQR you agree to make the code available under
the GPL License v3 or later.
