# MekoramaQR
# Copyright (C) 2017 by MekoramaQR contributors

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import unittest
from .. import get_resource
from StringIO import StringIO

from ..mekoqr import MekoQR, toHex

class TestMekoQR(unittest.TestCase):
    def test_read(self):
        infile = get_resource("..","test","codes","null.png")

        with open(infile, 'rb') as levelfile:
            qr = MekoQR(levelfile)

            self.assertIsNotNone(qr.code)

            self.assertEqual(16 * 16 * 16 + 25, len(qr.code))
            self.assertEqual("\tNew Level\x0eUnknown Author",qr.code[:25])
            for c in range(25, 16 * 16 * 16 + 25):
                self.assertEqual("\x00", qr.code[c])

    def test_read_str(self):
        infile = get_resource("..","test","codes","null.png")

        qr = MekoQR(infile)

        self.assertIsNotNone(qr.code)

        self.assertEqual(16 * 16 * 16 + 25, len(qr.code))
        self.assertEqual("\tNew Level\x0eUnknown Author",qr.code[:25])
        for c in range(25, 16 * 16 * 16 + 25):
            self.assertEqual("\x00", qr.code[c])

    def test_save(self):
        qr = MekoQR()

        # save test data
        outfile = StringIO()
        test = b'\00\01\02\03'
        hex = toHex(test)
        self.assertEqual("00010203",hex)
        img = qr.save(hex, outfile)
        self.assertIsNotNone(outfile.getvalue())
        self.assertIsNotNone(img)
        outfile.seek(0)

        # read anew
        qr = MekoQR()
        qr.read(outfile)
        self.assertEqual(test, qr.code)

    def test_save_code(self):
        infile = get_resource("..","test","codes","null.png")

        qr = MekoQR(infile)

        # save test data
        outfile = StringIO()
        img = qr.save(imgfile=outfile)
        self.assertIsNotNone(outfile.getvalue())
        self.assertIsNotNone(img)
        outfile.seek(0)

        # read anew
        qr2 = MekoQR()
        qr2.read(outfile)
        self.assertEqual(qr2.code, qr.code)


if __name__ == '__main__':
    unittest.main()